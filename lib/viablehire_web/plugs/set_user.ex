defmodule ViablehireWeb.Plugs.SetUser do
  import Plug.Conn

  alias Viablehire.Repo
  alias Viablehire.Accounts.User

  def init(_params) do
  end

  def call(conn, _params) do
    if conn.assigns[:user] do
      conn
    else
      user_id = get_session(conn, :user_id)
      assign(conn, :user, user_id && Repo.get(User, user_id))
    end
  end

end
