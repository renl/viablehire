defmodule ViablehireWeb.JobView do
  use ViablehireWeb, :view

  def render("scripts.html", %{load_datepicker: true}) do

    ~s|
    <script>$(\"#job_job_date\").datepicker({dateFormat: \"yy-mm-dd\"});</script>
      <script>require("js/app").SetTimeButtons.run();</script>
      |
    |> raw

    end

end
