defmodule ViablehireWeb.BidView do
  use ViablehireWeb, :view

  def render("scripts.html", %{load_conversation: true,
                               bid: bid,
                               user: user,
                               user_token: user_token}) do
    if bid.is_accepted do
      ~s{
        <script>window.userToken = "#{user_token}";</script>
        <script>window.bid_id = "#{bid.id}";</script>
        <script>window.nickname = "#{user.nickname}";</script>
        <script>require("js/app").Channel.run();</script>}
      |> raw
    end
  end
end
