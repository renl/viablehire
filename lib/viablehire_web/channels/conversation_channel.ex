defmodule ViablehireWeb.ConversationChannel do
  use ViablehireWeb, :channel

  alias Viablehire.Communications

  def join("conversation:bid_" <> _bid_id, _payload, socket) do
    if authorized?() do
      IO.inspect socket.assigns
      Communications.get_participant(
        %{
          user_id: socket.assigns.user_id,
          conversation_id: socket.assigns.conversation_id
        })
      |> Communications.update_participant_timestamp
      {:ok, socket}
    else
      {:error, %{reason: "Unauthorized"}}
    end
  end

  def handle_in("post", %{"msg" => msg, "from" => user}, socket) do
    case Communications.create_message_timestamp_conversation(
          %{
            sender_id: socket.assigns.user_id,
            conversation_id: socket.assigns.conversation_id,
            content: msg
          }) do
      {:ok, _} ->
        broadcast! socket, "post", %{msg: msg, from: user}
        {:noreply, socket}
      {:error, _failed_operation, _failed_value, _changes_so_far} ->
        {:reply, {:error, %{resp: "Error"}}, socket}
    end
  end

  def authorized? do
    true
  end
end
