defmodule ViablehireWeb.BidController do
  use ViablehireWeb, :controller

  alias Viablehire.Operations
  alias Viablehire.Operations.Bid
  alias Viablehire.Repo

  plug :check_user when action in [:delete, :update, :edit]
  plug :check_participant when action in [:show]

  defp check_participant(%{params: %{"id" => bid_id}} = conn, _params) do
    cond do
      Operations.get_bid!(bid_id) |> Map.get(:is_accepted) == false -> conn
      conn.assigns.user.id in Operations.get_bid_participants(bid_id) -> conn
      true ->
        conn
        |> put_flash(:error, "You cannot do that.")
        |> redirect(to: page_path(conn, :dashboard))
        |> halt()
    end
  end

  defp check_user(%{params: %{"id" => bid_id}} = conn, _params) do
    if Repo.get(Bid, bid_id).user_id == conn.assigns.user.id do
      conn
    else
      conn
      |> put_flash(:error, "You cannot do that.")
      |> redirect(to: page_path(conn, :dashboard))
      |> halt()
    end
  end

  def accept_bid(conn, %{"id" => id}) do
    results = Operations.get_bid!(id)
    |> Repo.preload(:job)
    |> Operations.accept_bid_and_setup_conversation

    case results do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Bid accepted successfully.")
        |> redirect(to: page_path(conn, :dashboard))
      {:error, _failed_operation, _failed_value, _changes_so_far} ->
        conn
        |> put_flash(:error, "Error accepting bid. Please try again.")
        |> redirect(to: page_path(conn, :dashboard))
    end
  end

  def unaccept_bid(conn, %{"id" => id}) do
    results = Operations.get_bid!(id)
    |> Repo.preload(:conversation)
    |> Operations.unaccept_bid_and_delete_conversation

    case results do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Bid is unaccepted.")
        |> redirect(to: page_path(conn, :dashboard))
      {:error, _failed_operation, _failed_value, _changes_so_far} ->
        conn
        |> put_flash(:error, "Error trying to unaccept bid.")
        |> redirect(to: page_path(conn, :dashboard))
    end
  end

  def index(conn, _params) do
    bids = Operations.list_bids()
    render(conn, "index.html", bids: bids)
  end

  def index_for_job(conn, %{"id" => job_id}) do
    bids = Operations.list_bids({:job, job_id})
    render(conn, "index.html", bids: bids)
  end

  def new(%{assigns: %{user: %{id: user_id}}} = conn, %{"id" => job_id}) do
    changeset = Operations.change_bid(%Bid{job_id: job_id, user_id: user_id})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"bid" => bid_params}) do
    bid_params = Map.put(bid_params, "is_accepted", false)
    case Operations.create_bid(bid_params) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Bid created successfully.")
        |> redirect(to: page_path(conn, :dashboard))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  defp get_user_token(conn, params) do
    Phoenix.Token.sign(conn, "user socket", params)
  end

  defp show_with_conversation(conn, bid) do
    user_token = get_user_token(
      conn,
      %{
        user_id: conn.assigns.user.id,
        bid_id: bid.id,
        conversation_id: bid.conversation.id,
        job_id: bid.job.id
      }
    )
    render(
      conn,
      "show.html",
      bid: bid,
      user: conn.assigns.user,
      load_conversation: bid.is_accepted,
      user_token: user_token
    )
  end

  def show(conn, %{"id" => id}) do
    bid = Operations.get_bid!(id)
    |> Repo.preload([user: [], job: [:user], conversation: [messages: [:user]]])
    if bid.is_accepted do
      show_with_conversation(conn, bid)
    else
      render conn, "show.html", bid: bid
    end
  end

  def edit(conn, %{"id" => id}) do
    bid = Operations.get_bid!(id)
    changeset = Operations.change_bid(bid)
    render(conn, "edit.html", bid: bid, changeset: changeset)
  end

  def update(conn, %{"id" => id, "bid" => bid_params}) do
    bid = Operations.get_bid!(id)

    case Operations.update_bid(bid, bid_params) do
      {:ok, bid} ->
        conn
        |> put_flash(:info, "Bid updated successfully.")
        |> redirect(to: bid_path(conn, :show, bid))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", bid: bid, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    bid = Operations.get_bid!(id)
    {:ok, _bid} = Operations.delete_bid(bid)

    conn
    |> put_flash(:info, "Bid deleted successfully.")
    |> redirect(to: page_path(conn, :dashboard))
  end
end
