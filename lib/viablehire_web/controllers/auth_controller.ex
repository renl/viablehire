defmodule ViablehireWeb.AuthController do
  use ViablehireWeb, :controller
  alias Viablehire.Repo
  alias Viablehire.Accounts.User
  plug Ueberauth

  def logout(conn, _params) do
    conn
    |> put_flash(:info, "You have been logged out.")
    |> configure_session(drop: true)
    |> redirect(to: "/")
  end

  def get_or_insert_user(changeset) do
    case Repo.get_by(User, email: changeset.changes.email) do
      nil -> Repo.insert(changeset)
      user -> {:ok, user}
    end
  end

  def new_or_old_user(conn, user) do
    user_params = %{email: user.email,
                    nickname: user.nickname}
    changeset = User.changeset(%User{}, user_params)
    if changeset.valid? do
      conn
      |> put_flash(:info, "Thanks for signing in.")
      |> put_session(:user_id, user.id)
      |> redirect(to: page_path(conn, :dashboard))
    else
      conn
      |> put_flash(:info, "Thanks for joining.")
      |> put_session(:user_id, user.id)
      |> redirect(to: user_path(conn, :edit, user.id))
    end
  end

  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
    changeset_auth0 = User.changeset_auth0(%User{}, %{email: auth.info.email})
    case get_or_insert_user(changeset_auth0) do
      {:ok, user} ->
        new_or_old_user(conn, user)
      {:error, _} ->
        conn
        |> put_flash(:error, "Error signing in.")
        |> redirect(to: "/")
    end
  end

  def callback(
    %{assigns: %{ueberauth_failure: _fails}} = conn,
    %{"error_description" => "Please verify your email before logging in."}) do
    IO.inspect conn
    conn
    |> put_flash(:info, "Need to verify email")
    |> redirect(to: "/")
  end

  def callback(%{assigns: %{ueberauth_failure: _fails}} = conn, _params) do
    IO.inspect conn
    conn
    |> put_flash(:error, "Auth error")
    |> redirect(to: "/")
  end
end
