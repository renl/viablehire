defmodule ViablehireWeb.PageController do
  use ViablehireWeb, :controller
  alias Viablehire.Accounts
  alias Viablehire.Operations

  def index(conn, _params) do
    if conn.assigns.user do
      redirect(conn, to: page_path(conn, :dashboard))
    else
      render conn, "index.html"
    end
  end

  def dashboard(conn, _params) do
    user = Accounts.get_all_user_participation(conn.assigns.user.id)
    participations_your_accepted_bids = Enum.filter user.participants, fn p ->
      p.conversation.bid.user_id == user.id
    end
    participations_bids_you_accepted = Enum.filter user.participants, fn p ->
      p.conversation.bid.job.user_id == user.id
    end

    user = Accounts.get_all_user_bids_and_jobs(conn.assigns.user.id)
    self_jobs = user.jobs
    |> Enum.map(fn %{bids: bids} = job ->
      Operations.calc_ave_bid_price(job, bids)
    end)
    self_bids_pending_acceptance = user.bids
    render(conn, "dashboard.html",
      self_jobs: self_jobs,
      self_bids_pending_acceptance: self_bids_pending_acceptance,
      participations_your_accepted_bids: participations_your_accepted_bids,
      participations_bids_you_accepted: participations_bids_you_accepted
    )
  end
end
