defmodule ViablehireWeb.JobController do
  use ViablehireWeb, :controller

  alias Viablehire.Operations
  alias Viablehire.Operations.Job
  alias Viablehire.Repo

  plug :check_user when action in [:delete, :update, :edit]

  defp check_user(%{params: %{"id" => job_id}} = conn, _params) do
    if Repo.get(Job, job_id).user_id == conn.assigns.user.id do
      conn
    else
      conn
      |> put_flash(:error, "You cannot do that.")
      |> redirect(to: user_path(conn, :show, conn.assigns.user.id))
      |> halt()
    end
  end

  def index(conn, _params) do
    all_jobs = Operations.list_jobs({:not_user, conn.assigns.user.id})
    |> Repo.preload(:bids)
    |> Enum.filter(fn job ->
      Enum.all?(job.bids, fn bid ->
        bid.user_id != conn.assigns.user.id
      end)
    end)
    |> Enum.map(fn %{bids: bids} = job ->
      Operations.calc_ave_bid_price(job, bids)
    end)
    render(conn, "index.html", jobs: all_jobs)
  end

  def new(conn, _params) do

    changeset = Operations.change_job(%Job{user_id: conn.assigns[:user].id})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"job" => job_params}) do
    IO.inspect job_params
    case Operations.create_job(job_params) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Job created successfully.")
        |> redirect(to: page_path(conn, :dashboard))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    job = Operations.get_job!(id)
    bids = Operations.list_bids({:job, job.id})
    job = Operations.calc_ave_bid_price job, bids
    self_bid = Operations.get_bid_by(%{user_id: conn.assigns.user.id, job_id: id})
    render(conn, "show.html", job: job, bids: bids, self_bid: self_bid)
  end

  def edit(conn, %{"id" => id}) do
    job = Operations.get_job!(id)
    changeset = Operations.change_job(job)
    IO.inspect changeset.data
    render(conn, "edit.html",
      job: job,
      load_datepicker: true,
      changeset: changeset)
  end

  def update(conn, %{"id" => id, "job" => job_params}) do

    job_params = job_params
    |> Map.update!("job_date", fn date ->
      Regex.named_captures(~r/(?<year>\d\d\d\d)\-(?<month>\d\d)\-(?<day>\d\d)/, date)
    end)
    IO.inspect job_params

    job = Operations.get_job!(id)

    case Operations.update_job(job, job_params) do
      {:ok, job} ->
        conn
        |> put_flash(:info, "Job updated successfully.")
        |> redirect(to: job_path(conn, :show, job))
      {:error, %Ecto.Changeset{} = changeset} ->
        IO.inspect changeset
        render(conn, "edit.html", job: job, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    job = Operations.get_job!(id)
    {:ok, _job} = Operations.delete_job(job)

    conn
    |> put_flash(:info, "Job deleted successfully.")
    |> redirect(to: page_path(conn, :dashboard))
  end
end
