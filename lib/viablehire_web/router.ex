defmodule ViablehireWeb.Router do
  use ViablehireWeb, :router

  pipeline :auth do
    plug ViablehireWeb.Plugs.RequireAuth
  end

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug ViablehireWeb.Plugs.SetUser
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/auth", ViablehireWeb do
    pipe_through :browser
    get "/logout", AuthController, :logout
    get "/:provider", AuthController, :request
    get "/:provider/callback", AuthController, :callback
    post "/:provider/callback", AuthController, :callback
  end

  scope "/users", ViablehireWeb do
    pipe_through [:browser, :auth]
    resources "/", UserController, except: [:index, :new, :create]
  end

  scope "/jobs", ViablehireWeb do
    pipe_through [:browser, :auth]
    resources "/", JobController
  end

  scope "/bids", ViablehireWeb do
    pipe_through [:browser, :auth]
    resources "/", BidController, except: [:new]
    get "/new/:id", BidController, :new
    get "/job/:id", BidController, :index_for_job
    get "/accept/:id", BidController, :accept_bid
    get "/unaccept/:id", BidController, :unaccept_bid
  end

  scope "/", ViablehireWeb do
    pipe_through :browser # Use the default browser stack
    get "/", PageController, :index
  end

  scope "/dashboard", ViablehireWeb do
    pipe_through [:browser, :auth]
    get "/", PageController, :dashboard
  end

  # Other scopes may use custom stacks.
  # scope "/api", ViablehireWeb do
  #   pipe_through :api
  # end
end
