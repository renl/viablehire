defmodule Viablehire.Operations do
  @moduledoc """
  The Operations context.
  """

  import Ecto.Query, warn: false
  alias Viablehire.Repo
  alias Viablehire.Communications
  alias Viablehire.Operations.Job
  alias Ecto.Multi

  @doc """
  Returns the list of jobs.

  ## Examples

      iex> list_jobs()
      [%Job{}, ...]

  """
  def list_jobs do
    Repo.all(Job)
  end

  def list_jobs({:user, user_id}) do
    Repo.all(from j in Job,
      where: j.user_id == ^user_id)
  end

  def list_jobs({:not_user, user_id}) do
    Repo.all(from j in Job,
      where: j.user_id != ^user_id)
  end

  @doc """
  Gets a single job.

  Raises `Ecto.NoResultsError` if the Job does not exist.

  ## Examples

      iex> get_job!(123)
      %Job{}

      iex> get_job!(456)
      ** (Ecto.NoResultsError)

  """
  def get_job!(id), do: Repo.get!(Job, id)

  @doc """
  Creates a job.

  ## Examples

      iex> create_job(%{field: value})
      {:ok, %Job{}}

      iex> create_job(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_job(attrs \\ %{}) do
    %Job{}
    |> Job.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a job.

  ## Examples

      iex> update_job(job, %{field: new_value})
      {:ok, %Job{}}

      iex> update_job(job, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_job(%Job{} = job, attrs) do
    job
    |> Job.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Job.

  ## Examples

      iex> delete_job(job)
      {:ok, %Job{}}

      iex> delete_job(job)
      {:error, %Ecto.Changeset{}}

  """
  def delete_job(%Job{} = job) do
    Repo.delete(job)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking job changes.

  ## Examples

      iex> change_job(job)
      %Ecto.Changeset{source: %Job{}}

  """
  def change_job(%Job{} = job) do
    Job.changeset(job, %{})
  end

  alias Viablehire.Operations.Bid

  @doc """
  Returns the list of bids.

  ## Examples

      iex> list_bids()
      [%Bid{}, ...]

  """
  def list_bids do
    Repo.all(Bid)
  end

  def list_bids({:user, user_id}) do
    Repo.all(from b in Bid,
      where: b.user_id == ^user_id)
  end

  def list_bids({:job, job_id}) do
    Repo.all(from b in Bid,
      where: b.job_id == ^job_id)
  end

  @doc """
  Gets a single bid.

  Raises `Ecto.NoResultsError` if the Bid does not exist.

  ## Examples

      iex> get_bid!(123)
      %Bid{}

      iex> get_bid!(456)
      ** (Ecto.NoResultsError)

  """
  def get_bid!(id), do: Repo.get!(Bid, id)

  def get_bid_by(%{user_id: user_id, job_id: job_id}) do
    Repo.get_by(Bid, user_id: user_id, job_id: job_id)
  end

  @doc """
  Creates a bid.

  ## Examples

      iex> create_bid(%{field: value})
      {:ok, %Bid{}}

      iex> create_bid(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_bid(attrs \\ %{}) do
    %Bid{}
    |> Bid.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a bid.

  ## Examples

      iex> update_bid(bid, %{field: new_value})
      {:ok, %Bid{}}

      iex> update_bid(bid, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_bid(%Bid{} = bid, attrs) do
    bid
    |> Bid.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Bid.

  ## Examples

      iex> delete_bid(bid)
      {:ok, %Bid{}}

      iex> delete_bid(bid)
      {:error, %Ecto.Changeset{}}

  """
  def delete_bid(%Bid{} = bid) do
    Repo.delete(bid)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking bid changes.

  ## Examples

      iex> change_bid(bid)
      %Ecto.Changeset{source: %Bid{}}

  """
  def change_bid(%Bid{} = bid) do
    Bid.changeset(bid, %{})
  end

  def accept_bid_and_setup_conversation(bid) do
    Multi.new
    |> Multi.run(:updated_bid, fn _ ->
      update_bid(bid, %{is_accepted: true})
    end)
    |> Multi.run(:conversation, fn %{updated_bid: updated_bid} ->
      Communications.create_conversation(%{bid_id: updated_bid.id})
    end)
    |> Multi.run(:participant_bidder, fn %{conversation: conversation} ->
      Communications.create_participant(%{conversation_id: conversation.id,
                                          user_id: bid.user_id})
    end)
    |> Multi.run(:participant_poster, fn %{participant_bidder: bidder} ->
      Communications.create_participant(%{conversation_id: bidder.conversation_id,
                                          user_id: bid.job.user_id})
    end)
    |> Repo.transaction
  end

  def unaccept_bid_and_delete_conversation(bid) do
    Multi.new
    |> Multi.run(:updated_bid, fn _ ->
      update_bid(bid, %{is_accepted: false})
    end)
    |> Multi.run(:delete_conversation, fn _ ->
      Communications.delete_conversation(bid.conversation)
    end)
    |> Repo.transaction
  end

  def get_bid_participants(bid_id) do
    from(b in Bid,
      where: b.id == ^bid_id,
      join: c in assoc(b, :conversation),
      join: p in assoc(c, :participants),
      select: p.user_id)
    |> Repo.all
  end

  def calc_ave_bid_price(job, bids) do
    bids_count = Enum.count(bids)
    bid_ave_price = if bids_count > 0 do
      Enum.reduce(bids, Decimal.new(0), fn bid, acc -> Decimal.add(acc, bid.bid_price) end)
      |> Decimal.div(bids_count)
      |> Decimal.round(2)
      |> Decimal.to_string(:normal)
    else
      "0.00"
    end
    Map.merge(job,
      %{
        bids_count: bids_count,
        bid_ave_price: bid_ave_price
      })
  end
end
