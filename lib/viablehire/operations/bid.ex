defmodule Viablehire.Operations.Bid do
  use Ecto.Schema
  import Ecto.Changeset


  schema "bids" do
    field :bid_price, :decimal
    field :comment, :string
    field :is_accepted, :boolean, default: false
    belongs_to :user, Viablehire.Accounts.User
    belongs_to :job, Viablehire.Operations.Job
    has_one :conversation, Viablehire.Communications.Conversation

    timestamps()
  end

  @doc false
  def changeset(bid, attrs) do
    bid
    |> cast(attrs, [:bid_price, :comment, :is_accepted, :job_id, :user_id])
    |> validate_required([:bid_price, :comment, :is_accepted, :job_id, :user_id])
  end
end
