defmodule Viablehire.Operations.Job do
  use Ecto.Schema
  import Ecto.Changeset


  schema "jobs" do
    field :address, :string
    field :description, :string
    field :duration, :integer
    field :job_date, :date
    field :job_time, :time
    field :number_of_hires, :integer
    field :title, :string
    belongs_to :user, Viablehire.Accounts.User
    has_many :bids, Viablehire.Operations.Bid

    timestamps()
  end

  @doc false
  def changeset(job, attrs) do
    job
    |> cast(attrs, [:title, :description, :job_date, :job_time, :address, :duration, :number_of_hires, :user_id])
    |> validate_required([:title, :description, :job_date, :job_time, :address, :duration, :number_of_hires, :user_id])
  end
end
