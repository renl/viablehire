defmodule Viablehire.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset


  schema "users" do
    field :nickname, :string
    field :email, :string
    field :first_name, :string
    field :last_name, :string
    has_many :jobs, Viablehire.Operations.Job
    has_many :bids, Viablehire.Operations.Bid
    has_many :participants, Viablehire.Communications.Participant
    has_many :messages, Viablehire.Communications.Message, foreign_key: :sender_id

    timestamps()
  end

  @doc false
  def changeset_auth0(user, attrs) do
    user
    |> cast(attrs, [:email])
    |> validate_required([:email])
    |> unique_constraint(:email)
  end

  def changeset(user, attrs) do
    user
    |> cast(attrs, [:nickname, :first_name, :last_name, :email])
    |> validate_required([:nickname, :email])
    |> unique_constraint(:email)
    |> unique_constraint(:nickname)
  end
end
