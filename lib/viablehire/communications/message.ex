defmodule Viablehire.Communications.Message do
  use Ecto.Schema
  import Ecto.Changeset


  schema "messages" do
    field :content, :string
    belongs_to :user, Viablehire.Accounts.User, foreign_key: :sender_id
    belongs_to :conversation, Viablehire.Communications.Conversation

    timestamps()
  end

  @doc false
  def changeset(message, attrs) do
    message
    |> cast(attrs, [:content, :sender_id, :conversation_id])
    |> validate_required([:content, :sender_id, :conversation_id])
    |> foreign_key_constraint(:conversation_id)
    |> foreign_key_constraint(:sender_id)
  end
end
