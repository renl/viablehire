defmodule Viablehire.Communications.Conversation do
  use Ecto.Schema
  import Ecto.Changeset


  schema "conversations" do
    belongs_to :bid, Viablehire.Operations.Bid
    has_many :participants, Viablehire.Communications.Participant
    has_many :messages, Viablehire.Communications.Message

    timestamps()
  end

  @doc false
  def changeset(conversation, attrs) do
    conversation
    |> cast(attrs, [:bid_id])
    |> validate_required([:bid_id])
  end
end
