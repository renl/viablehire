defmodule Viablehire.Communications.Participant do
  use Ecto.Schema
  import Ecto.Changeset


  schema "participants" do
    belongs_to :user, Viablehire.Accounts.User
    belongs_to :conversation, Viablehire.Communications.Conversation

    timestamps()
  end

  @doc false
  def changeset(participant, attrs) do
    participant
    |> cast(attrs, [:user_id, :conversation_id])
    |> validate_required([:user_id, :conversation_id])
  end
end
