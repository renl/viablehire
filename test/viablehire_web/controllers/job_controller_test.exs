defmodule ViablehireWeb.JobControllerTest do
  use ViablehireWeb.ConnCase

  @create_attrs %{
    address: "some address",
    description: "some description",
    duration: 42,
    job_date: ~D[2010-04-17],
    job_time: ~T[14:00:00.000000],
    number_of_hires: 42,
    title: "some title"
  }
  @update_attrs %{
    address: "some updated address",
    description: "some updated description",
    duration: 43,
    job_date: ~D[2011-05-18],
    job_time: ~T[15:01:01.000000],
    number_of_hires: 43,
    title: "some updated title"
  }
  @invalid_attrs %{
    address: nil,
    description: nil,
    duration: nil,
    job_date: nil,
    job_time: nil,
    number_of_hires: nil,
    title: nil
  }

  # def fixture(:job, user_id) do
  #   {:ok, job} = Operations.create_job(Map.put(@create_attrs, :user_id, user_id))
  #   job
  # end

  describe "index" do
    test "lists all jobs", %{conn: conn} do
      conn = get conn, job_path(conn, :index)
      assert html_response(conn, 200) =~ "All Available Jobs"
    end
  end

  describe "new job" do
    test "renders form", %{conn: conn} do
      conn = get conn, job_path(conn, :new)
      assert html_response(conn, 200) =~ "New Job"
    end
  end

  describe "create job" do
    test "redirects to show when data is valid", %{conn: conn, user: user} do
      conn = post conn, job_path(conn, :create), job: Map.put(@create_attrs, :user_id, user.id)
      assert "/dashboard" = redir_path = redirected_to(conn, 302)
      conn = get recycle(conn), redir_path
      assert html_response(conn, 200) =~ "Job created successfully"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, job_path(conn, :create), job: @invalid_attrs
      assert html_response(conn, 200) =~ "New Job"
    end
  end

  describe "edit job" do
    # setup [:create_job]

    test "renders form for editing chosen job", %{conn: conn, job: job} do
      conn = get conn, job_path(conn, :edit, job)
      assert html_response(conn, 200) =~ "Edit Job"
    end
  end

  describe "update job" do
    # setup [:create_job]

    test "redirects when data is valid", %{conn: conn, job: job} do
      conn = put conn, job_path(conn, :update, job), job: @update_attrs
      assert redirected_to(conn) == job_path(conn, :show, job)

      conn = get conn, job_path(conn, :show, job)
      assert html_response(conn, 200) =~ "some updated address"
    end

    test "renders errors when data is invalid", %{conn: conn, job: job} do
      conn = put conn, job_path(conn, :update, job), job: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Job"
    end
  end

  describe "delete job" do
    # setup [:create_job]

    test "deletes chosen job", %{conn: conn, job: job} do
      conn = delete conn, job_path(conn, :delete, job)
      assert redirected_to(conn) == page_path(conn, :dashboard)
      assert_error_sent 404, fn ->
        get conn, job_path(conn, :show, job)
      end
    end
  end

  # defp create_job(%{user: user}) do
  #   job = fixture(:job, user.id)
  #   {:ok, job: job}
  # end
end
