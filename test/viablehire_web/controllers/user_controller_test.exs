defmodule ViablehireWeb.UserControllerTest do
  use ViablehireWeb.ConnCase

  @update_attrs %{
    email: "some updated email",
    first_name: "some updated first_name",
    last_name: "some updated last_name",
    nickname: "some updated phone"
  }
  @invalid_attrs %{
    email: nil,
    first_name: nil,
    last_name: nil,
    nickname: nil
  }

  # def fixture(:user) do
  #   {:ok, user} = Accounts.create_user(@create_attrs)
  #   user
  # end

  describe "edit user" do
    #setup [:create_user]

    test "renders form for editing chosen user", %{conn: conn, user: user} do
      conn = get conn, user_path(conn, :edit, user)
      assert html_response(conn, 200) =~ "Edit User"
    end
  end

  describe "update user" do
    #setup [:create_user]

    test "redirects when data is valid", %{conn: conn, user: user} do
      conn = put conn, user_path(conn, :update, user), user: @update_attrs
      assert redirected_to(conn) == page_path(conn, :dashboard)

      conn = get conn, user_path(conn, :show, user)
      assert html_response(conn, 200) =~ "some updated email"
    end

    test "renders errors when data is invalid", %{conn: conn, user: user} do
      conn = put conn, user_path(conn, :update, user), user: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit User"
    end
  end

  describe "delete user" do
    #setup [:create_user]

    test "deletes chosen user", %{conn: conn, user: user} do
      conn = delete conn, user_path(conn, :delete, user)
      assert redirected_to(conn) == page_path(conn, :index)
      get conn, user_path(conn, :show, user)
      assert redirected_to(conn) == page_path(conn, :index)
    end
  end

  describe "show user" do
    #setup [:create_user]

    test "show chosen user", %{conn: conn, user: user} do
      conn = get conn, user_path(conn, :show, user)
      assert html_response(conn, 200) =~ "Show User"
   end
  end

  # defp create_user(%{conn: conn}) do
  #   user = fixture(:user)
  #   conn = conn
  #   |> Plug.Test.init_test_session(user_id: user.id)
  #   {:ok, user: user, conn: conn}
  # end
end
