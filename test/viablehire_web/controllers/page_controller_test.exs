defmodule ViablehireWeb.PageControllerTest do
  use ViablehireWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert "/dashboard" = redir_path = redirected_to(conn, 302)
    conn = get recycle(conn), redir_path
    assert html_response(conn, 200) =~ "Viablehire"
  end
end
