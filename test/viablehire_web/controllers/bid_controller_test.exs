defmodule ViablehireWeb.BidControllerTest do
  use ViablehireWeb.ConnCase

  @create_attrs %{
    bid_price: "120.5",
    comment: "some comment",
  }
  @update_attrs %{
    bid_price: "456.7",
    comment: "some updated comment",
    is_accepted: false
  }
  @invalid_attrs %{
    bid_price: nil,
    comment: nil,
    is_accepted: nil
  }

  describe "index" do
    test "lists all bids", %{conn: conn} do
      conn = get conn, bid_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Bids"
    end

    test "lists all bids for a specific job", %{conn: conn, job: job} do
      conn = get conn, bid_path(conn, :index_for_job, job)
      assert html_response(conn, 200) =~ "Listing Bids"
    end
  end

  describe "accept bids" do
    test "accepting a job", %{conn: conn, bid: bid} do
      conn = get conn, bid_path(conn, :accept_bid, bid)
      assert "/dashboard" = redir_path = redirected_to(conn, 302)
      conn = get recycle(conn), redir_path
      assert html_response(conn, 200) =~ "Bid accepted successfully."
    end

    test "unaccepting a job", %{conn: conn, bid: bid} do
      conn = get conn, bid_path(conn, :unaccept_bid, bid)
      assert "/dashboard" = redir_path = redirected_to(conn, 302)
      conn = get recycle(conn), redir_path
      assert html_response(conn, 200) =~ "Bid is unaccepted."
    end
  end

  describe "new bid" do
    test "renders form", %{conn: conn, job: job} do
      conn = get conn, bid_path(conn, :new, job)
      assert html_response(conn, 200) =~ "New Bid"
    end
  end

  describe "create bid" do
    test "redirects to show when data is valid", %{conn: conn, user: user, job: job} do
      params = @create_attrs
      |> Map.put(:user_id, user.id)
      |> Map.put(:job_id, job.id)

      conn = post conn, bid_path(conn, :create), bid: params
      assert "/dashboard" = redir_path = redirected_to(conn, 302)
      conn = get recycle(conn), redir_path
      assert html_response(conn, 200) =~ "Bid created successfully"

    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, bid_path(conn, :create), bid: @invalid_attrs
      assert html_response(conn, 200) =~ "New Bid"
    end
  end

  describe "edit bid" do
    test "renders form for editing chosen bid", %{conn: conn, bid: bid} do
      conn = get conn, bid_path(conn, :edit, bid)
      assert html_response(conn, 200) =~ "Edit Bid"
    end
  end

  describe "update bid" do
    test "redirects when data is valid", %{conn: conn, bid: bid} do
      conn = put conn, bid_path(conn, :update, bid), bid: @update_attrs
      assert redirected_to(conn) == bid_path(conn, :show, bid)

      conn = get conn, bid_path(conn, :show, bid)
      assert html_response(conn, 200) =~ "some updated comment"
    end

    test "renders errors when data is invalid", %{conn: conn, bid: bid} do
      conn = put conn, bid_path(conn, :update, bid), bid: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Bid"
    end
  end

  describe "delete bid" do
    test "deletes chosen bid", %{conn: conn, bid: bid} do
      conn = delete conn, bid_path(conn, :delete, bid)
      assert redirected_to(conn) == page_path(conn, :dashboard)
      assert_error_sent 404, fn ->
        get conn, bid_path(conn, :show, bid)
      end
    end
  end
end
