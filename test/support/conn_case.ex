defmodule ViablehireWeb.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build common datastructures and query the data layer.

  Finally, if the test case interacts with the database,
  it cannot be async. For this reason, every test runs
  inside a transaction which is reset at the beginning
  of the test unless the test case is marked as async.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with connections
      use Phoenix.ConnTest
      import ViablehireWeb.Router.Helpers

      # The default endpoint for testing
      @endpoint ViablehireWeb.Endpoint
    end
  end

  alias Viablehire.Accounts
  alias Viablehire.Operations
  alias Viablehire.Communications

  @user_create_attrs %{
    email: "some email",
    first_name: "some first_name",
    last_name: "some last_name",
    nickname: "some phone"
  }
  @job_create_attrs %{
    address: "some address",
    description: "some description",
    duration: 42,
    job_date: ~D[2010-04-17],
    job_time: ~T[14:00:00.000000],
    number_of_hires: 42,
    title: "some title"
  }
  @bid_create_attrs %{
    bid_price: "120.5",
    comment: "some comment",
  }

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Viablehire.Repo)
    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Viablehire.Repo, {:shared, self()})
    end
    {:ok, user} = Accounts.create_user(@user_create_attrs)
    {:ok, job} = @job_create_attrs
    |> Map.put(:user_id, user.id)
    |> Operations.create_job
    {:ok, bid} = @bid_create_attrs
    |> Map.put(:user_id, user.id)
    |> Map.put(:job_id, job.id)
    |> Operations.create_bid
    {:ok, conversation} = Communications.create_conversation(%{bid_id: bid.id})
    conn = Phoenix.ConnTest.build_conn()
    |> Plug.Test.init_test_session(user_id: user.id)
    {:ok, conn: conn, user: user, job: job, bid: bid, conversation: conversation}
  end

end
