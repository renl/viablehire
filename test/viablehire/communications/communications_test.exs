defmodule Viablehire.CommunicationsTest do
  use Viablehire.DataCase

  alias Viablehire.Communications
  alias Viablehire.Operations
  alias Viablehire.Accounts
  alias Viablehire.Communications.Conversation
  alias Viablehire.Communications.Participant
  alias Viablehire.Communications.Message
  alias Viablehire.Repo

    @user_valid_attrs %{
      email: "some email",
      first_name: "some first_name",
      last_name: "some last_name",
      nickname: "some phone"
    }
    @job_valid_attrs %{
      address: "some address",
      description: "some description",
      duration: 42,
      job_date: ~D[2010-04-17],
      job_time: ~T[14:00:00.000000],
      number_of_hires: 42,
      title: "some title"
    }
    @bid_valid_attrs %{
      bid_price: "120.5",
      comment: "some comment",
      is_accepted: true
    }
    @conversation_valid_attrs %{}
#    @conversation_update_attrs %{}
    @conversation_invalid_attrs %{
      bid_id: nil
    }
    @participant_valid_attrs %{}
    @participant_update_attrs %{}
    @participant_invalid_attrs %{
      conversation_id: nil,
      user_id: nil
    }

    @message_valid_attrs %{content: "some content"}
    @message_update_attrs %{content: "some updated content"}
    @message_invalid_attrs %{content: nil}

    def message_fixture(attrs \\ %{}) do
      conversation = conversation_fixture()
      |> Repo.preload([bid: [job: [user: []]]])
      {:ok, message} =
        attrs
        |> Enum.into(@message_valid_attrs)
        |> Enum.into(%{conversation_id: conversation.id, sender_id: conversation.bid.job.user_id})
        |> Communications.create_message()

      message
    end


    def participant_fixture(attrs \\ %{}) do
      conversation = conversation_fixture()
      |> Repo.preload([bid: [job: [user: []]]])
      {:ok, participant} =
        attrs
        |> Enum.into(@participant_valid_attrs)
        |> Enum.into(%{conversation_id: conversation.id, user_id: conversation.bid.job.user_id})
        |> Communications.create_participant()

      participant
    end

    def user_fixture() do
      {:ok, user} = Accounts.create_user(@user_valid_attrs)
      user
    end

    def job_fixture(attrs \\ %{}) do
      {:ok, job} =
        attrs
        |> Enum.into(@job_valid_attrs)
        |> Enum.into(%{user_id: user_fixture().id})
        |> Operations.create_job()

      job
    end

    def bid_fixture(attrs \\ %{}) do
      job = job_fixture()
      {:ok, bid} =
        attrs
        |> Enum.into(@bid_valid_attrs)
        |> Enum.into(%{job_id: job.id, user_id: job.user_id})
        |> Operations.create_bid()

      bid
    end

    def conversation_fixture(attrs \\ %{}) do
      bid = bid_fixture()
      {:ok, conversation} =
        attrs
        |> Enum.into(@conversation_valid_attrs)
        |> Enum.into(%{bid_id: bid.id})
        |> Communications.create_conversation()

      conversation
    end

  describe "conversations" do

    test "list_conversations/0 returns all conversations" do
      conversation = conversation_fixture()
      assert Communications.list_conversations() == [conversation]
    end

    test "get_conversation!/1 returns the conversation with given id" do
      conversation = conversation_fixture()
      assert Communications.get_conversation!(conversation.id) == conversation
    end

    test "create_conversation/1 with valid data creates a conversation" do
      bid = bid_fixture()
      assert {:ok, %Conversation{}} = Communications.create_conversation(%{bid_id: bid.id})
    end

    test "create_conversation/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Communications.create_conversation(@conversation_invalid_attrs)
    end

    test "update_conversation/2 with invalid data returns error changeset" do
      conversation = conversation_fixture()
      assert {:error, %Ecto.Changeset{}} = Communications.update_conversation(conversation, %{bid_id: nil})
      assert conversation == Communications.get_conversation!(conversation.id)
    end

    test "delete_conversation/1 deletes the conversation" do
      conversation = conversation_fixture()
      assert {:ok, %Conversation{}} = Communications.delete_conversation(conversation)
      assert_raise Ecto.NoResultsError, fn -> Communications.get_conversation!(conversation.id) end
    end

    test "change_conversation/1 returns a conversation changeset" do
      conversation = conversation_fixture()
      assert %Ecto.Changeset{} = Communications.change_conversation(conversation)
    end
  end

  describe "participants" do

    test "list_participants/0 returns all participants" do
      participant = participant_fixture()
      assert Communications.list_participants() == [participant]
    end

    test "get_participant!/1 returns the participant with given id" do
      participant = participant_fixture()
      assert Communications.get_participant!(participant.id) == participant
    end

    test "create_participant/1 with valid data creates a participant" do
      conversation = conversation_fixture()
      |> Repo.preload([bid: [job: [user: []]]])
      attrs = @participant_valid_attrs
      |> Enum.into(%{conversation_id: conversation.id, user_id: conversation.bid.job.user_id})
      assert {:ok, %Participant{}} = Communications.create_participant(attrs)
    end

    test "create_participant/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Communications.create_participant(@participant_invalid_attrs)
    end

    test "update_participant/2 with valid data updates the participant" do
      participant = participant_fixture()
      assert {:ok, participant} = Communications.update_participant(participant, @participant_update_attrs)
      assert %Participant{} = participant
    end

    test "update_participant/2 with invalid data returns error changeset" do
      participant = participant_fixture()
      assert {:error, %Ecto.Changeset{}} = Communications.update_participant(participant, @participant_invalid_attrs)
      assert participant == Communications.get_participant!(participant.id)
    end

    test "delete_participant/1 deletes the participant" do
      participant = participant_fixture()
      assert {:ok, %Participant{}} = Communications.delete_participant(participant)
      assert_raise Ecto.NoResultsError, fn -> Communications.get_participant!(participant.id) end
    end

    test "change_participant/1 returns a participant changeset" do
      participant = participant_fixture()
      assert %Ecto.Changeset{} = Communications.change_participant(participant)
    end
  end

  describe "messages" do
    test "list_messages/0 returns all messages" do
      message = message_fixture()
      assert Communications.list_messages() == [message]
    end

    test "get_message!/1 returns the message with given id" do
      message = message_fixture()
      assert Communications.get_message!(message.id) == message
    end

    test "create_message/1 with valid data creates a message" do
      conversation = conversation_fixture()
      |> Repo.preload([bid: [job: [user: []]]])
      attrs = @message_valid_attrs
      |> Enum.into(%{conversation_id: conversation.id, sender_id: conversation.bid.job.user_id})
      assert {:ok, %Message{} = message} = Communications.create_message(attrs)
      assert message.content == "some content"
    end

    test "create_message/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Communications.create_message(@message_invalid_attrs)
    end

    test "update_message/2 with valid data updates the message" do
      message = message_fixture()
      assert {:ok, message} = Communications.update_message(message, @message_update_attrs)
      assert %Message{} = message
      assert message.content == "some updated content"
    end

    test "update_message/2 with invalid data returns error changeset" do
      message = message_fixture()
      assert {:error, %Ecto.Changeset{}} = Communications.update_message(message, @message_invalid_attrs)
      assert message == Communications.get_message!(message.id)
    end

    test "delete_message/1 deletes the message" do
      message = message_fixture()
      assert {:ok, %Message{}} = Communications.delete_message(message)
      assert_raise Ecto.NoResultsError, fn -> Communications.get_message!(message.id) end
    end

    test "change_message/1 returns a message changeset" do
      message = message_fixture()
      assert %Ecto.Changeset{} = Communications.change_message(message)
    end
  end
end
