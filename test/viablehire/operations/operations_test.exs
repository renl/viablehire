defmodule Viablehire.OperationsTest do
  use Viablehire.DataCase

  alias Viablehire.Operations
  alias Viablehire.Accounts


  alias Viablehire.Operations.Job

  @user_valid_attrs %{
    email: "some email",
    first_name: "some first_name",
    last_name: "some last_name",
    nickname: "some phone"
  }
  @job_valid_attrs %{
    address: "some address",
    description: "some description",
    duration: 42,
    job_date: ~D[2010-04-17],
    job_time: ~T[14:00:00.000000],
    number_of_hires: 42,
    title: "some title"
  }
  @job_update_attrs %{
    address: "some updated address",
    description: "some updated description",
    duration: 43,
    job_date: ~D[2011-05-18],
    job_time: ~T[15:01:01.000000],
    number_of_hires: 43,
    title: "some updated title"
  }
  @job_invalid_attrs %{
    address: nil,
    description: nil,
    duration: nil,
    job_date: nil,
    job_time: nil,
    number_of_hires: nil,
    title: nil
  }

  @bid_valid_attrs %{
    bid_price: "120.5",
    comment: "some comment",
    is_accepted: true
  }
  @bid_update_attrs %{
    bid_price: "456.7",
    comment: "some updated comment",
    is_accepted: false
  }
  @bid_invalid_attrs %{
    bid_price: nil,
    comment: nil,
    is_accepted: nil
  }

  def user_fixture() do
    {:ok, user} = Accounts.create_user(@user_valid_attrs)
    user
  end

  def job_fixture(attrs \\ %{}) do
    {:ok, job} =
      attrs
      |> Enum.into(@job_valid_attrs)
      |> Enum.into(%{user_id: user_fixture().id})
      |> Operations.create_job()

    job
  end

  def bid_fixture(attrs \\ %{}) do
    job = job_fixture()
    {:ok, bid} =
      attrs
      |> Enum.into(@bid_valid_attrs)
      |> Enum.into(%{job_id: job.id, user_id: job.user_id})
      |> Operations.create_bid()

    bid
  end


  describe "jobs" do


    test "list_jobs/0 returns all jobs" do
      job = job_fixture()
      assert Operations.list_jobs() == [job]
    end

    test "get_job!/1 returns the job with given id" do
      job = job_fixture()
      assert Operations.get_job!(job.id) == job
    end

    test "create_job/1 with valid data creates a job" do
      attrs = Enum.into(@job_valid_attrs, %{user_id: user_fixture().id})
      assert {:ok, %Job{} = job} = Operations.create_job(attrs)
      assert job.address == "some address"
      assert job.description == "some description"
      assert job.duration == 42
      assert job.job_date == ~D[2010-04-17]
      assert job.job_time == ~T[14:00:00.000000]
      assert job.number_of_hires == 42
      assert job.title == "some title"
    end

    test "create_job/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Operations.create_job(@job_invalid_attrs)
    end

    test "update_job/2 with valid data updates the job" do
      job = job_fixture()
      assert {:ok, job} = Operations.update_job(job, @job_update_attrs)
      assert %Job{} = job
      assert job.address == "some updated address"
      assert job.description == "some updated description"
      assert job.duration == 43
      assert job.job_date == ~D[2011-05-18]
      assert job.job_time == ~T[15:01:01.000000]
      assert job.number_of_hires == 43
      assert job.title == "some updated title"
    end

    test "update_job/2 with invalid data returns error changeset" do
      job = job_fixture()
      assert {:error, %Ecto.Changeset{}} = Operations.update_job(job, @job_invalid_attrs)
      assert job == Operations.get_job!(job.id)
    end

    test "delete_job/1 deletes the job" do
      job = job_fixture()
      assert {:ok, %Job{}} = Operations.delete_job(job)
      assert_raise Ecto.NoResultsError, fn -> Operations.get_job!(job.id) end
    end

    test "change_job/1 returns a job changeset" do
      job = job_fixture()
      assert %Ecto.Changeset{} = Operations.change_job(job)
    end
  end

  describe "bids" do
    alias Viablehire.Operations.Bid

    test "list_bids/0 returns all bids" do
      bid = bid_fixture()
      assert Operations.list_bids() == [bid]
    end

    test "get_bid!/1 returns the bid with given id" do
      bid = bid_fixture()
      assert Operations.get_bid!(bid.id) == bid
    end

    test "create_bid/1 with valid data creates a bid" do
      job = job_fixture()
      attrs = Enum.into(@bid_valid_attrs, %{job_id: job.id, user_id: job.user_id})
      assert {:ok, %Bid{} = bid} = Operations.create_bid(attrs)
      assert bid.bid_price == Decimal.new("120.5")
      assert bid.comment == "some comment"
      assert bid.is_accepted == true
    end

    test "create_bid/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Operations.create_bid(@bid_invalid_attrs)
    end

    test "update_bid/2 with valid data updates the bid" do
      bid = bid_fixture()
      assert {:ok, bid} = Operations.update_bid(bid, @bid_update_attrs)
      assert %Bid{} = bid
      assert bid.bid_price == Decimal.new("456.7")
      assert bid.comment == "some updated comment"
      assert bid.is_accepted == false
    end

    test "update_bid/2 with invalid data returns error changeset" do
      bid = bid_fixture()
      assert {:error, %Ecto.Changeset{}} = Operations.update_bid(bid, @bid_invalid_attrs)
      assert bid == Operations.get_bid!(bid.id)
    end

    test "delete_bid/1 deletes the bid" do
      bid = bid_fixture()
      assert {:ok, %Bid{}} = Operations.delete_bid(bid)
      assert_raise Ecto.NoResultsError, fn -> Operations.get_bid!(bid.id) end
    end

    test "change_bid/1 returns a bid changeset" do
      bid = bid_fixture()
      assert %Ecto.Changeset{} = Operations.change_bid(bid)
    end
  end
end
