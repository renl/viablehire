export var Time_adjust = {
    time_adjust: function(){
        $("#timeup").click(function(){
            let hour = $("#job_job_time_hour")[0];
            let minute = $("#job_job_time_minute")[0];
            minute.selectedIndex++;
            if(minute.selectedIndex == -1) {
                minute.selectedIndex = 0;
                hour.selectedIndex++;
                if(hour.selectedIndex == -1) {
                    hour.selectedIndex = 0;
                }
            }
        });
        $("#timedown").click(function(){
            let hour = $("#job_job_time_hour")[0];
            let minute = $("#job_job_time_minute")[0];
            minute.selectedIndex--;
            if(minute.selectedIndex == -1) {
                minute.selectedIndex = 1;
                hour.selectedIndex--;
                if(hour.selectedIndex == -1) {
                    hour.selectedIndex = 23;
                }
            }
        });
    }
};
