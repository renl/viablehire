import {Socket} from "phoenix";

export var Conversation = {
    conversation: function() {

        let socket = new Socket("/socket", {params: {token: window.userToken}});
        let msg_board = document.getElementById("msg-board");

        socket.connect();
        let channel = socket.channel("conversation:bid_" + window.bid_id, {});

        channel.join()
            .receive("ok", resp => { console.log("Joined successfully", resp); })
            .receive("error", resp => { console.log("Unable to join", resp); });


        document.querySelector("#msg-input").addEventListener("submit", (e) => {
            e.preventDefault();
            let msg_content = e.target.querySelector("#msg-content");
            channel.push("post", {
                from: window.nickname,
                msg: msg_content.value})
                .receive("error", resp => { alert(resp.resp)});
            msg_content.value = "";
        });

        channel.on("post", (msg) => {
            let msg_div;
            if(window.nickname === msg.from) {
                let t = document.querySelector("#self-msg");
                msg_div = document.importNode(t.content, true);
                let span = msg_div.querySelectorAll("span");
                span[0].textContent = msg.from;
                span[1].textContent = msg.msg;
            } else {
                let t = document.querySelector("#other-msg");
                msg_div = document.importNode(t.content, true);
                let span = msg_div.querySelectorAll("span");
                span[1].textContent = msg.from;
                span[0].textContent = msg.msg;
            }
            msg_board.appendChild(msg_div);
            console.log(msg.msg);
        });

    }
};
