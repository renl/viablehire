defmodule Viablehire.Repo.Migrations.CreateConversations do
  use Ecto.Migration

  def change do
    create table(:conversations) do
      add :bid_id, references(:bids, on_delete: :delete_all)
      timestamps()
    end
    create index(:conversations, [:bid_id])
  end
end
