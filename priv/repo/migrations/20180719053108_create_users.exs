defmodule Viablehire.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :nickname, :string
      add :first_name, :string
      add :last_name, :string
      add :email, :string

      timestamps()
    end

    create unique_index(:users, [:nickname, :email])
  end
end
