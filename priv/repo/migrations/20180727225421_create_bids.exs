defmodule Viablehire.Repo.Migrations.CreateBids do
  use Ecto.Migration

  def change do
    create table(:bids) do
      add :bid_price, :decimal
      add :comment, :string
      add :is_accepted, :boolean, default: false, null: false
      add :job_id, references(:jobs, on_delete: :delete_all)
      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end

    create index(:bids, [:job_id])
    create index(:bids, [:user_id])
  end
end
