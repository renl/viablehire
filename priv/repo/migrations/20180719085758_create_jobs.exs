defmodule Viablehire.Repo.Migrations.CreateJobs do
  use Ecto.Migration

  def change do
    create table(:jobs) do
      add :title, :string
      add :description, :string
      add :job_date, :date
      add :job_time, :time
      add :address, :string
      add :duration, :integer
      add :number_of_hires, :integer
      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end

    create index(:jobs, [:user_id])
  end
end
